package com.rinno.lab.launcher2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/**
 * Created by oso on 07-11-15.
 */
public class RunScreenSaverReceiver extends BroadcastReceiver {

    private static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";
    private static final String ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN";

    public void onReceive(Context ctx, Intent intent) {
        RinnoLogger.i("*************RunScreenSaverReceiver");

        if (BOOT_COMPLETED.equals(intent.getAction())) {
            RinnoLogger.i("*************BOOT_COMPLETED");

            try {
                String category=RinnoConfig.getConfig(ctx).getCategoryApp();
                String name=RinnoConfig.getConfig(ctx).getNameApp();

                RinnoLogger.i("*************ejecutar DownloadService");
                PackageManager manager = ctx.getPackageManager();
                Intent i = manager.getLaunchIntentForPackage(name);
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ctx.startActivity(i);
            }catch (Exception e){
                RinnoLogger.e("*************Error DownloadService:"+e.getMessage(),e);
            }



        }
        if (ACTION_SHUTDOWN.equals(intent.getAction())) {
            RinnoLogger.i("*************ACTION_SHUTDOWN");
        }
    }
}
