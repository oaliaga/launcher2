package com.rinno.lab.launcher2;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;

/**
 * Created by oso on 12-11-15.
 */
public class ScreenSaverService extends Service
{
    public IBinder onBind(Intent paramIntent)
    {
        return null;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
    {
        BroadcastReceiver local1 = new BroadcastReceiver()
        {
            @SuppressWarnings("deprecation")
            public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
            {

                RinnoLogger.i("onReceive action:"+paramAnonymousIntent.getAction());

                PowerManager localPowerManager = (PowerManager)ScreenSaverService.this.getSystemService(Context.POWER_SERVICE);
                KeyguardManager localKeyguardManager = (KeyguardManager)ScreenSaverService.this.getSystemService(Context.KEYGUARD_SERVICE);
                if (!localPowerManager.isScreenOn())
                {
                    PowerManager.WakeLock localWakeLock = localPowerManager.newWakeLock( 805306394, "Your App Tag");
                    localWakeLock.acquire();
                    localWakeLock.release();
                    localKeyguardManager.newKeyguardLock("Your App Tag").disableKeyguard();
                    Intent localIntent = new Intent(ScreenSaverService.this.getApplicationContext(), MainActivity.class);
                    localIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    ScreenSaverService.this.startActivity(localIntent);
                }
            }
        };
        IntentFilter localIntentFilter = new IntentFilter("android.intent.action.SCREEN_OFF");
        try
        {
            registerReceiver(local1, localIntentFilter);
            return Service.START_NOT_STICKY;
        }
        catch (RuntimeException localRuntimeException)
        {
            return Service.START_NOT_STICKY;
        }
    }
}

