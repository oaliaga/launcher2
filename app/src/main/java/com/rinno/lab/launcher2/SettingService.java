package com.rinno.lab.launcher2;

/**
 * Created by oso on 15-11-15.
 */



import android.app.ActivityManager;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;




public class SettingService  extends Service {

    private final IBinder mBinder = new LocalBinder();
    private boolean run=true;

    public class LocalBinder extends Binder {
        SettingService getService() {
            return SettingService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {
        run=false;
    }
    public void onCreate() {

        super.onCreate();

        RinnoLogger.i("create SettingService");

        new Thread(new Runnable() {

            @Override
            public void run() {
                //String old="";
                while(run){
                    String packageName;
                    String className;
                    ActivityManager amen = (ActivityManager)getBaseContext().getSystemService(ACTIVITY_SERVICE);
                    if(Build.VERSION.SDK_INT > 20){
                        packageName = amen.getRunningAppProcesses().get(0).processName;
                    }
                    else{
                        packageName = amen.getRunningTasks(1).get(0).topActivity.getPackageName();
                    }
                    className = amen.getRunningTasks(1).get(0).topActivity.getClassName();

                    if (!Setting.getInstance().isPaso()){
                        //EntelLogger.i("2"+packageName+" "+className+"  old:"+old+" className:"+className);
                        if(
                                ("com.android.packageinstaller.UninstallerActivity".equals(className)||
                                        "com.android.settings.GridSettings".equals(className)||
                                        "com.android.settings.Settings".equals(className)||
                                        "com.android.settings".equals(packageName)||
                                        "com.android.vending".equals(packageName)
                                )//&&!old.equals(className)
                                ){
                            //EntelLogger.i(packageName+" "+className);
                            Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }
                    }
                    //old=className;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {

                    }
                }
            }
        }).start();
    }

}

