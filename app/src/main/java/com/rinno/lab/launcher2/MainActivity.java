package com.rinno.lab.launcher2;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.provider.Settings.System.putInt(this.getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, 30000);


        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);


        Intent localIntent2 = new Intent(getApplicationContext(), SettingService.class);
        getApplicationContext().startService(localIntent2);


    }

    @Override
    protected void onResume() {
        super.onResume();
        String category=RinnoConfig.getConfig(getApplicationContext()).getCategoryApp();
        String name=RinnoConfig.getConfig(getApplicationContext()).getNameApp();
        RinnoLogger.i("*************category:"+category+" name:"+name);
        if(!RinnoConfig.getConfig(getApplicationContext()).isOk()&&(category==null||"".equals(category))){
            Intent i = new Intent(this, AppsListActivity.class);
            startActivity(i);
        }else{
            try {
                RinnoLogger.i("*************ejecutar getLaunchIntentForPackage");
                PackageManager manager = getPackageManager();

                Intent i = manager.getLaunchIntentForPackage(name);
                i.addCategory(Intent.CATEGORY_LAUNCHER);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }catch (Exception e){
                RinnoLogger.e("*************Error getLaunchIntentForPackage:"+e.getMessage(),e);
            }
        }

    }

    private List<AppDetail> loadApps(){
        PackageManager manager = getPackageManager();
        List<AppDetail> apps = new ArrayList<AppDetail>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for(ResolveInfo ri:availableActivities){
            AppDetail app = new AppDetail();
            app.label = ri.loadLabel(manager);
            app.name = ri.activityInfo.packageName;
            RinnoLogger.i("*************app.label:"+app.label+" app.name:"+app.name );
            app.icon = ri.activityInfo.loadIcon(manager);
            apps.add(app);
        }
        return apps;
    }
}
