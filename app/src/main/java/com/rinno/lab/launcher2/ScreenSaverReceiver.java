package com.rinno.lab.launcher2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by oso on 12-11-15.
 */
public class ScreenSaverReceiver  extends BroadcastReceiver {
    public void onReceive(Context ctx, Intent paramIntent) {

        try{
            RinnoLogger.i("paramIntent action:"+paramIntent.getAction());
            RinnoLogger.i("isOk:"+RinnoConfig.getConfig(ctx).isOk());
            if (RinnoConfig.getConfig(ctx).isOk() )
                ctx.startService(new Intent(ctx, ScreenSaverService.class));

        }catch(Exception e){
            RinnoLogger.i("Error_boot:"+e.getMessage());
        }
    }
}
