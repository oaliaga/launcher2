package com.rinno.lab.launcher2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by oso on 15-11-15.
 */
public class SettingActivity  extends Activity {

    private EditText clave = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        startService(new Intent(getApplicationContext(), SettingService.class));

        clave = (EditText) findViewById(R.id.editTextClave);
        Button button = (Button) findViewById(R.id.buttonClave);

        button.setOnClickListener(new ButonOnClickListener());
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private class ButonOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            String valor = clave.getText().toString();
            if ("123456".equals(valor)) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date());
                cal.add(Calendar.SECOND, 45);

                Setting.getInstance().setPaso(cal.getTimeInMillis());
                Intent intent = new Intent(Settings.ACTION_SETTINGS);

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        }
    }
}

