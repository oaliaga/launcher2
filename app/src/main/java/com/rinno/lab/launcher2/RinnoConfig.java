package com.rinno.lab.launcher2;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by oso on 07-11-15.
 */
public class RinnoConfig {
    private Context ctx;

    private RinnoConfig(Context ctx) {
        this.ctx = ctx;
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        this.ok = settings.getBoolean(RinnoConfig.OK, false);
        this.nameApp= settings.getString(RinnoConfig.NAME_APP, "");
        this.categoryApp= settings.getString(RinnoConfig.CATEGORY_APP, "");
    }

    private void saveString(String key, String value){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private String getString(String key,String defaultValue){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ctx);
        return settings.getString(key, defaultValue);
    }


    public static final String OK="OK";
    public static final String NAME_APP="NAME_APP";
    public static final String CATEGORY_APP="CATEGORY_APP";

    private static RinnoConfig cachedInstance = null;
    private boolean ok  ;
    private String nameApp;
    private String categoryApp;


    public static synchronized RinnoConfig getConfig(Context ctx) {
        if (cachedInstance == null) {
            synchronized (RinnoConfig.class) {
                if (cachedInstance == null)
                    cachedInstance = new RinnoConfig(ctx);
            }
        }
        return cachedInstance;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok=ok;
    }

    public String getNameApp() {
        return nameApp;
    }

    public void setNameApp(String nameApp) {
        saveString(NAME_APP,nameApp);
        this.nameApp = nameApp;
    }

    public String getCategoryApp() {
        return categoryApp;
    }

    public void setCategoryApp(String categoryApp) {
        saveString(CATEGORY_APP,nameApp);
        this.categoryApp = categoryApp;
    }
}
