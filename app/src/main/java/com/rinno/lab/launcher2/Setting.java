package com.rinno.lab.launcher2;

/**
 * Created by oso on 15-11-15.
 */

import java.util.Date;

public class Setting {



    private long time=0;
    public static Setting instance=null;
    private Setting(){}


    public static Setting getInstance(){
        if(instance==null){
            instance=new Setting();
        }
        return instance;
    }


    public boolean isPaso() {
        if (time==0)
            return false;
        return (time>new Date().getTime());

    }


    public void setPaso(long time) {
        this.time = time;
    }



}

