package com.rinno.lab.launcher2;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oso on 07-11-15.
 */
public class AppsListActivity extends Activity {

    private TextView nombre_app;
    private TextView cat_app;
    private PackageManager manager;
    private List<AppDetail> apps;
    private Button btn_select_app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apps_list);
        loadApps();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadListView();
        list = (ListView)findViewById(R.id.apps_list);
        nombre_app=(TextView)findViewById(R.id.nombre_app);
        cat_app=(TextView)findViewById(R.id.cat_app);

        findViewById(R.id.btn_select_app);

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> av, View v, int pos,
                                            long id) {
                        String catApp = apps.get(pos).label.toString();
                        String nameApp = apps.get(pos).name.toString();


                        nombre_app.setText(nameApp);
                        cat_app.setText(catApp);

                        RinnoConfig.getConfig(getApplicationContext()).setCategoryApp(catApp);
                        RinnoConfig.getConfig(getApplicationContext()).setNameApp(nameApp);
                    }
                });
        btn_select_app=(Button)findViewById(R.id.btn_select_app);
        btn_select_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String cat=RinnoConfig.getConfig(getApplicationContext()).getCategoryApp();
                String name=RinnoConfig.getConfig(getApplicationContext()).getNameApp();
                RinnoLogger.i("app:" + name + " " + cat);
                RinnoConfig.getConfig(getApplicationContext()).setOk(true);
                finish();
            }
        });
    }



    private void loadApps(){
        manager = getPackageManager();
        apps = new ArrayList<>();

        Intent i = new Intent(Intent.ACTION_MAIN, null);
        i.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> availableActivities = manager.queryIntentActivities(i, 0);
        for(ResolveInfo ri:availableActivities){
            AppDetail app = new AppDetail();
            app.label = ri.loadLabel(manager);
            app.name = ri.activityInfo.packageName;
            app.icon = ri.activityInfo.loadIcon(manager);
            apps.add(app);
        }
    }

    private ListView list;

    private void loadListView(){
        list = (ListView)findViewById(R.id.apps_list);

        ArrayAdapter<AppDetail> adapter = new ArrayAdapter<AppDetail>(this,
                R.layout.list_item,
                apps) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    convertView = getLayoutInflater().inflate(R.layout.list_item, null);
                }

                ImageView appIcon = (ImageView)convertView.findViewById(R.id.item_app_icon);
                appIcon.setImageDrawable(apps.get(position).icon);

                TextView appLabel = (TextView)convertView.findViewById(R.id.item_app_label);
                appLabel.setText(apps.get(position).label);

                TextView appName = (TextView)convertView.findViewById(R.id.item_app_name);
                appName.setText(apps.get(position).name);

                return convertView;
            }
        };

        list.setAdapter(adapter);
    }

}
