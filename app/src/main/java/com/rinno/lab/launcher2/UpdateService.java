package com.rinno.lab.launcher2;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.PowerManager;

/**
 * Created by oso on 14-11-15.
 */
public class UpdateService extends Service {

    public IBinder onBind(Intent paramIntent)
    {
        return null;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }
    @Override
    public void onCreate() {
        super.onCreate();
        // REGISTER RECEIVER THAT HANDLES SCREEN ON AND SCREEN OFF LOGIC
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        boolean screenOn = intent.getBooleanExtra("screen_state", false);
        if (!screenOn) {
            // YOUR CODE
            RinnoLogger.i("!screenOn");
        } else {
            // YOUR CODE
            RinnoLogger.i("_screenOn");

            PowerManager localPowerManager = (PowerManager)this.getSystemService(Context.POWER_SERVICE);
            KeyguardManager localKeyguardManager = (KeyguardManager)this.getSystemService(Context.KEYGUARD_SERVICE);
            if (!localPowerManager.isScreenOn())
            {
                PowerManager.WakeLock localWakeLock = localPowerManager.newWakeLock( 805306394, "Your App Tag");
                localWakeLock.acquire();
                localWakeLock.release();
                localKeyguardManager.newKeyguardLock("Your App Tag").disableKeyguard();
                Intent localIntent = new Intent(this.getApplicationContext(), MainActivity.class);
                localIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(localIntent);
            }
        }
    }
}
