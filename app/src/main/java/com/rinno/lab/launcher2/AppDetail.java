package com.rinno.lab.launcher2;

/**
 * Created by oso on 07-11-15.
 */
import android.graphics.drawable.Drawable;

public class AppDetail {
    public CharSequence label;
    public CharSequence name;
    public Drawable icon;
}

