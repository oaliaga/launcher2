package com.rinno.lab.launcher2;

import android.util.Log;

/**
 * Created by oso on 07-11-15.
 */
public class RinnoLogger {



    private static final String TAG="RINNO";

    public static void d(String message) {
        //if (PreyConfig.LOG_DEBUG_ENABLED)
        Log.i(TAG, message);
    }

    public static void i(String message) {
        Log.i(TAG,message);
    }

    public static void e(final String message, Throwable e) {
        if (e!=null)
            Log.e(TAG, message, e);
        else
            Log.e(TAG, message);
    }


}